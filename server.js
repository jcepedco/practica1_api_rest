var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
var baseMlaBURL = "https://api.mlab.com/api/1/databases/apitechujjcc/collections/";
var mLabAPIKey = "apiKey=xAE7mMaafnYS6IQDSRH1oKnuZHkqCa8Z";

var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto BIP BIP BIP" + port);

app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send(
      {
        "msg" : "Bienvenido a la API de Tech Univerity molona"
      }
    )
  }
)

app.get('/apitechu/v1/users',
  function(req, res)
  {
    console.log("GET /apitechu/v1/users");
    //res.sendFile('usuarios.json', {root: __dirname});
    res.sendfile('./usuarios.json');
  }
)
app.get('/apitechu/v2/users',
  function(req, res)
  {
    console.log("GET /apitechu/v2/users");
    httpClient = requestJson.createClient(baseMlaBURL);
    console.log("Cliente creado");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMlab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      })
  }
)

app.get('/apitechu/v2/users/:id',
  function(req, res)
  {
    console.log("GET /apitechu/v2/users/:id");
    var id = req.params.id;
    var query = 'q={"id": ' + id + '}';

    httpClient = requestJson.createClient(baseMlaBURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        response = {"msg" : "Error obteniendo usuario."}
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body[0];
          res.send(response);
        } else {
          response = {"msg" : "Usuario no encontrado."};
          res.status(404);
          res.send(response);
     }
   }
  })
  }
)

app.get('/apitechu/v2/users/:id/accounts',
  function(req, res)
  {
    console.log("GET /apitechu/v2/users/:id/accounts");
    var userid = req.params.id;
    var query = 'q={"UserId": ' + userid + '}';

    httpClient = requestJson.createClient(baseMlaBURL);
    console.log("Cliente creado");
    console.log("account?" + query + "&" + mLabAPIKey);
    httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        response = {"msg" : "Error obteniendo cuentas."}
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          response = body;
          res.send(response);
        } else {
          response = {"msg" : "Cuenta no encontrada."};
          res.status(404);
          res.send(response);
     }
   }
  })
  }
)

app.post('/apitechu/v1/users',
  function(req, res){
  console.log("POST /apitechu/v1/users");
  //console.log(req);
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "country" : req.body.country
  };
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("country es " + req.body.country);

  var users = require('./usuarios.json');
  users.push(newUser);

  writeUserDataToFile(users);

  //console.log("Usuario añadido con éxito");
  res.send(users);
  //res.send(
  //  {"msg" : "Usuario añadido con éxito"}
}
)

app.delete('/apitechu/v1/users/:id',
  function(req, res){
    console.log("DELETE /apitechu/v1/users/");
    var users = require('./usuarios.json');
    users.splice(req.params.id -1, 1);
    writeUserDataToFile(users);
    res.send(users);
}
)
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile(
    "./usuarios.json",
    jsonUserData,
    "utf8",
    function(err) {
      if (err){
        console.log(err);
      }else{
        console.log("Usuario persistido");
      }
    }
  );
}


app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

}
)

app.post('/apitechu/v1/login',
  function(req,res){
    console.log("POST /apitechu/v1/login");
    var users = require('./usuarios_login.json');
    var encontrado = false;
    var ident = 0;
    console.log(req.body.email);
    console.log(req.body.password);
    for (user of users) {
      if (user.email == req.body.email && user.password == req.body.password)
        {
          encontrado = true;
          ident = user.id;
          user.logged = true;
          break;
        }
    }
    if (encontrado) {
      writeUserDataToFile2('./usuarios_login.json',users);
      res.send({
        "mensaje" : "Login correcto",
        "idUsuario" : ident
      });
    }
    else {
      res.send({
        "mensaje" : "Login incorrecto"
      });
    }
  })

  app.post('/apitechu/v2/login',
    function(req,res){
    var email = req.body.email;
    var password = req.body.password;
    var query = 'q={"email":"' + email +'","password":"' + password +'"}';
    httpClient = requestJson.createClient(baseMlaBURL);
    console.log("Cliente creado");
    console.log(baseMlaBURL +"user?" + query + "&" + mLabAPIKey);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log(err);
      console.log(body);
    if (err) {
      response = {"msg" : "Error obteniendo usuario."};
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
          var query2='q={"id":'+body[0].id+'}';
          var putBody ='{"$set":{"logged":true}}';
          response = body[0];
          console.log("user?"+query2+'&'+mLabAPIKey);
          console.log(putBody);
          httpClient.put("user?"+query2+'&'+mLabAPIKey, JSON.parse(putBody),
          function(errPut,resMlabPut,bodyPut){
              console.log('ERROR PUT:'+errPut);
              console.log('BODY PUT:'+bodyPut);
              res.send(response);
            });
      } else {
        response = {"msg" : "Usuario no encontrado."};
        res.status(404);
        res.send(response);
   }
 }
})
}
)

app.post('/apitechu/v2/logout',
  function(req,res){
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"' + email +'","logged":true}';
  httpClient = requestJson.createClient(baseMlaBURL);
  console.log("Cliente creado");
  console.log(baseMlaBURL +"user?" + query + "&" + mLabAPIKey);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    console.log(err);
    console.log(body);
  if (err) {
    response = {"msg" : "Error obteniendo usuario."};
    res.status(500);
  } else {
    if (body.length > 0) {
        var query2='q={"id":'+body[0].id+'}';
        var putBody ='{"$unset":{"logged":""}}';
        response = body[0];
        console.log("user?"+query2+'&'+mLabAPIKey);
        console.log(putBody);
        httpClient.put("user?"+query2+'&'+mLabAPIKey, JSON.parse(putBody),
        function(errPut,resMlabPut,bodyPut){
            console.log('ERROR PUT:'+errPut);
            console.log('BODY PUT:'+bodyPut);
            res.send(response);
          });
    } else {
      response = {"msg" : "Usuario no encontrado."};
      res.status(404);
 }
}
res.send(response);
})
}
)


  app.post('/apitechu/v1/loginEn',
    function(req,res){
      console.log("POST /apitechu/v1/loginEn");
      var users = require('./usuarios_login.json');
      var encontrado = false;
      console.log(req.body.id);
      for (user of users) {
        if (user.id == req.body.id)
          {
            encontrado = true;
            delete user.logged;
            break;
          }
      }
      if (encontrado) {

        writeUserDataToFile2('./usuarios_login.json',users);
        res.send({
          "mensaje" : "Logout correcto"
        });
      }
      else {
        res.send({
          "mensaje" : "Logout incorrecto"
        });
      }
    })

    function writeUserDataToFile2(fichero,data){
      var fs = require('fs');
      var jsonUserData = JSON.stringify(data);
      fs.writeFile(
        fichero,
        jsonUserData,
        "utf8",
        function(err) {
          if (err){
            console.log(err);
          }else{
            console.log("Usuario persistido");
          }
        }
      );
    }
